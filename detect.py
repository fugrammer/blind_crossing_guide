from __future__ import division

import cv2
import numpy as np

class LaneDetector:

    def __init__(self, road_horizon=50, detection_horizon=300):
        self.prob_hough = True
        self.vote = 65
        self.roi_theta = 0.35
        self.detection_horizon = detection_horizon
        self.road_horizon = road_horizon

    def _detection_distance(self, x1, y1, x2, y2, width):
        # compute the point where the give line crosses the base of the frame
        # return distance of that point from center of the frame
        if x2 == x1:
            return (width * 0.5) - x1
        m = (y2 - y1) / (x2 - x1)
        c = y1 - m * x1
        detection_cross = (self.detection_horizon-c)/m
        return (width * 0.5) - detection_cross

    def get_direction(self,line1,line2,width):
        threshold = 50
        x1 = self._detection_distance(line1[0],line1[1],line1[2],line1[3],width)
        x2 = self._detection_distance(line2[0],line2[1],line2[2],line2[3],width)
        if (x1+x2)/2>threshold:
            #print ("Go left")
            return 0
        elif (x1+x2)/2<-threshold:
            #print ("Go right")
            return 1
        else:
            #print ("Go straight")
            return 2

    def _scale_line(self, x1, y1, x2, y2, frame_height):
        # scale the farthest point of the segment to be on the drawing horizon
        if x1 == x2:
            if y1 < y2:
                y1 = self.road_horizon
                y2 = frame_height
                return x1, y1, x2, y2
            else:
                y2 = self.road_horizon
                y1 = frame_height
                return x1, y1, x2, y2
        if y1 < y2:
            m = (y1 - y2) / (x1 - x2)
            x1 = ((self.road_horizon - y1) / m) + x1
            y1 = self.road_horizon
            x2 = ((frame_height - y2) / m) + x2
            y2 = frame_height
        else:
            m = (y2 - y1) / (x2 - x1)
            x2 = ((self.road_horizon - y2) / m) + x2
            y2 = self.road_horizon
            x1 = ((frame_height - y1) / m) + x1
            y1 = frame_height
        return x1, y1, x2, y2

    def detect(self, frame,low,high):

        img = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #(thresh, img) = cv2.threshold(img, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        #thresh = 170
        #frame = cv2.threshold(frame, thresh, 255, cv2.THRESH_BINARY)[1]
        roiy_end = frame.shape[0]
        roix_end = frame.shape[1]
        roi = img[self.road_horizon:roiy_end, 0:roix_end]
        blur = cv2.medianBlur(roi, 5)
        contours = cv2.Canny(blur, low, high) # (60,120)
        lines = cv2.HoughLinesP(contours, 1, np.pi / 180, self.vote, minLineLength=50, maxLineGap=50)
        if lines is not None:
            # find nearest lines to center
            lines = lines + np.array([0, self.road_horizon, 0, self.road_horizon]).reshape(
                (1, 1, 4))  # scale points from ROI coordinates to full frame coordinates
            left_bound = None
            right_bound = None
            #answer = []
            for l in lines:
                for x1,y1,x2,y2 in l:
                    theta = np.abs(np.arctan2((y2 - y1), (x2 - x1)))  # line angle WRT horizon
                    if theta>self.roi_theta:
                        #answer += [[x1,y1,x2,y2]]
                        dist = self._detection_distance(x1, y1, x2, y2, frame.shape[1])
                        if left_bound is None and dist < 0:
                            left_bound = (x1, y1, x2, y2)
                            left_dist = dist
                        elif right_bound is None and dist > 0:
                            right_bound = (x1, y1, x2, y2)
                            right_dist = dist
                        elif left_bound is not None and 0 > dist > left_dist:
                            left_bound = (x1, y1, x2, y2)
                            left_dist = dist
                        elif right_bound is not None and 0 < dist < right_dist:
                            right_bound = (x1, y1, x2, y2)
                            right_dist = dist
            if left_bound is not None:
                left_bound = self._scale_line(left_bound[0], left_bound[1], left_bound[2], left_bound[3],
                                              frame.shape[0])
            if right_bound is not None:
                right_bound = self._scale_line(right_bound[0], right_bound[1], right_bound[2], right_bound[3],
                                               frame.shape[0])
            return [left_bound, right_bound]
            #return answer

