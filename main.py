from __future__ import division

import cv2
import detect
import Tkinter as tk
from Tkinter import *
from collections import Counter
from AudioAlerter import AudioAlerter
import math
val = True
flag = 1

def most_common(lst):
    data = Counter(lst)
    return data.most_common(1)[0][0]

def main(image_path,audio,root,labels):

    LOW = 200
    HIGH = 300
    DETECTION_HORIZON = 300
    ROAD_HORIZON = 50
    ALPHA = 5

    filenames = ["./files/complicated_crossing.mov","./files/sidewalk.mov","./files/approach_crossing_straight_cross.mov"]
    ld = detect.LaneDetector(ROAD_HORIZON, DETECTION_HORIZON)
    cap = cv2.VideoCapture(filenames[0])
    direction_history = []
    lline_history=[]
    rline_history=[]
    while True:
        #frame = cv2.imread(image_path)
        ret, frame = cap.read()
        frame = cv2.resize(frame, (640, 480), interpolation=cv2.INTER_CUBIC)
        lanes = ld.detect(frame,LOW,HIGH)
        #frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #(thresh, frame) = cv2.threshold(frame, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        #blur = cv2.medianBlur(frame, 5)
        #contours = cv2.Canny(blur, LOW, HIGH)
        #frame = contours
        if lanes is not None:
            if lanes[1] is not None:
                x1,y1,x2,y2 = lanes[1]
                m = (y2 - y1) / (x2 - x1)
                c = y1 - m * x1
                if len(lline_history) == ALPHA:
                    lline_history=lline_history[1:ALPHA]
                lline_history+=[lanes[1]]
            if lanes[0] is not None:
                x1,y1,x2,y2 = lanes[0]
                m = (y2 - y1) / (x2 - x1)
                c = y1 - m * x1
                if len(rline_history)==ALPHA:
                    rline_history=rline_history[1:ALPHA]
                rline_history+=[lanes[0]]
            if lanes[0] is not None and lanes[1] is not None:
                for label in labels:
                    label.config(bg="white")
                if len(direction_history)==ALPHA:
                    direction_history=direction_history[1:ALPHA]
                direction_history+=[ld.get_direction(lanes[0],lanes[1],frame.shape[1])]
                audio.setflag(most_common(direction_history))
                #labels[most_common(direction_history)].config(bg="red")
                #root.update()
            if len(lline_history) > 0:
                laverage_x1 = sum(line[0] for line in lline_history) / len(lline_history)
                laverage_y1 = sum(line[1] for line in lline_history) / len(lline_history)
                laverage_x2 = sum(line[2] for line in lline_history) / len(lline_history)
                laverage_y2 = sum(line[3] for line in lline_history) / len(lline_history)
                try:
                    cv2.line(frame, (int(laverage_x1), int(laverage_y1)), (int(laverage_x2), int(laverage_y2)), (0, 0, 255), 5)
                except:
                    pass
            if len(rline_history) > 0:
                laverage_x1 = sum(line[0] for line in rline_history) / len(rline_history)
                laverage_y1 = sum(line[1] for line in rline_history) / len(rline_history)
                laverage_x2 = sum(line[2] for line in rline_history) / len(rline_history)
                laverage_y2 = sum(line[3] for line in rline_history) / len(rline_history)
                try:
                    cv2.line(frame, (int(laverage_x1), int(laverage_y1)), (int(laverage_x2), int(laverage_y2)),
                             (0, 0, 255), 5)
                except:
                    pass
            # for line in lanes:
            #     if line != None:
            #         cv2.line(frame,(int(line[0]),int(line[1])),(int(line[2]),int(line[3])),(125,125,125),5)

        cv2.imshow('', frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            audio.setval(False)
            break

def initialise():
    root=tk.Tk()
    root.title("Navi")
    labels=[]
    labels+=[tk.Label(root,bg="white",text="Left")]
    labels+=[tk.Label(root,bg="white",text="Straight")]
    labels+=[tk.Label(root,bg="white",text="Right")]
    labels[0].pack(padx=5,pady=5,side=LEFT)
    labels[1].pack(padx=5,pady=5,side=LEFT)
    labels[2].pack(padx=5,pady=5,side=LEFT)
    for i in labels:
        i.config(font="Times 100 bold")
        i.pack()
    a = AudioAlerter("alert")
    a.start()
    return a,root,labels

a,root,labels=initialise()
main("./files/complicated1.jpg",a,root,labels)