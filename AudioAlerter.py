import threading
import pyaudio
import math
import numpy
import time

BITRATE = 44100
FREQUENCY = 300
LENGTH = 0.05

delay = 0.5


class AudioAlerter(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(channels=2,
            rate=44100,
            format=pyaudio.paFloat32,
            output=True)
        NUMBEROFFRAMES = int(BITRATE * LENGTH)
        RESTFRAMES = NUMBEROFFRAMES % BITRATE
        self.WAVEDATA = []
        for x in xrange(NUMBEROFFRAMES):
            self.WAVEDATA += [(int(math.sin(x / ((BITRATE / FREQUENCY) / math.pi)) * 127))]
        for x in xrange(RESTFRAMES):
            self.WAVEDATA += [0]
        self.val = True
        self.flag = 2

    def setval(self,val):
        self.val = val

    def setflag(self,flag):
        self.flag = flag

    def run(self):
        while self.val==True:
            stereo_signal = numpy.zeros([len(self.WAVEDATA), 2])  # these two lines are new
            if self.flag == 0:
                stereo_signal[:, 0] = self.WAVEDATA[:]  # 1 for right speaker, 0 for  left
            elif self.flag == 1:
                stereo_signal[:, 1] = self.WAVEDATA[:]
            else:
                stereo_signal[:, 0] = self.WAVEDATA[:]
                stereo_signal[:, 1] = self.WAVEDATA[:]
            chunks = []
            chunks.append(stereo_signal)
            chunk = numpy.concatenate(chunks) * 0.1
            self.stream.write(chunk.astype(numpy.float32).tostring())
            time.sleep(delay)
        self.stream.stop_stream()
        self.stream.close()
        self.p.terminate()